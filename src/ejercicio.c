/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

/*=====[Inclusions of function dependencies]=================================*/

#include "ejercicio.h"

#include "asm_module.h"
#include "c_function.h"
#include "uartPrint.h"
#include "sapi.h"

/*=====[Definition macros of private constants]==============================*/

#ifndef elementsof
	#define elementsof(x)  (sizeof(x) / sizeof((x)[0]))
#endif

/*=====[Definitions of extern global variables]==============================*/

/*=====[Definitions of public global variables]==============================*/

/*=====[Definitions of private global variables]=============================*/

/*=====[Main function, program entry point after power on or reset]==========*/

int main(void) {

	/*=====[Definitions of private local variables]=============================*/
	uint16_t asm_vector[]={ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
			12, 13, 14, 15, 16, 17,	18, 19, 20, 21, 22, 23, 24, 25,
			26 };
	uint16_t c_vector[]={ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11,
			12, 13, 14, 15, 16, 17,	18, 19, 20, 21, 22, 23, 24, 25,
			26 };
	uint16_t asm_vectorOut[elementsof(asm_vector)];
	uint16_t c_vectorOut[elementsof(c_vector)];
	uint32_t cyclesElapsed = 0;

// ----- Setup -----------------------------------
	boardInit();

   // Configura el contador de ciclos con el clock de la EDU-CIAA NXP
   cyclesCounterConfig(EDU_CIAA_NXP_CLOCK_SPEED);

   uartWriteString( UART_USB, "asm_vector = ");
   uartWriteArraytoStringLF(UART_USB, asm_vector, elementsof(asm_vector));

   // Resetea el contador de ciclos
   cyclesCounterReset();

   //Ejecuta funcion asm
   asm_productoEscalar12(asm_vector, asm_vectorOut, elementsof(asm_vector), 190);

   // Guarda en una variable los ciclos leidos
   cyclesElapsed = cyclesCounterRead();
   uartWriteString( UART_USB, "La funcion asm se ejecuto en ");
   uartWriteInttoString(UART_USB, cyclesElapsed);
   uartWriteString( UART_USB, " ciclos\n\r");

   uartWriteString( UART_USB, "El resultado fue asm_vector = ");
   uartWriteArraytoStringLF(UART_USB, asm_vectorOut, elementsof(asm_vectorOut));

   uartWriteString( UART_USB, "c_vector = {");
   uartWriteArraytoStringLF(UART_USB, c_vector, elementsof(c_vector));

   // Resetea el contador de ciclos
   cyclesCounterReset();

   //Ejecuta funcion C
   c_productoEscalar16(c_vector, c_vectorOut, elementsof(c_vector), 190);

   // Guarda en una variable los ciclos leidos
   cyclesElapsed = cyclesCounterRead();
   uartWriteString( UART_USB, "La funcion C se ejecuto en ");
   uartWriteInttoString(UART_USB, cyclesElapsed);
   uartWriteString( UART_USB, " ciclos\n\r");

   uartWriteString( UART_USB, "El resultado fue c_vector = ");
   uartWriteArraytoStringLF(UART_USB, c_vectorOut, elementsof(c_vectorOut));

// ----- Repeat for ever -------------------------
	while ( true) {
		__WFI(); //wfi
	}

	// YOU NEVER REACH HERE, because this program runs directly or on a
	// microcontroller and is not called by any Operating System, as in the
	// case of a PC program.
	return 0;
}
