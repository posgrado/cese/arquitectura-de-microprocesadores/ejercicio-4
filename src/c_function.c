/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#include "c_function.h"

void c_productoEscalar16(uint16_t * vectorIn, uint16_t * vectorOut, uint32_t longitud, uint32_t escalar){
	for (uint32_t var = 0; var < longitud; ++var) {
		uint32_t mul = vectorIn[var] * escalar;
		if (mul < 4096) {
			vectorOut[var] = mul;
		} else {
			vectorOut[var] = 4095;
		}
	}
}

