# Compile options
VERBOSE=n
OPT=g
USE_NANO=y
SEMIHOST=n
USE_FPU=y

MODULES+=$(sort $(dir $(wildcard _modules/*/)))