/*=============================================================================
 * Copyright (c) 2020, David Broin <davidmbroin@gmail.com>
 * All rights reserved.
 * License: mit (see LICENSE.txt)
 * Date: 2020/05/30
 *===========================================================================*/

#ifndef __EJERCICIO_C_FUNCTION_H__
#define __EJERCICIO_C_FUNCTION_H__

#include "ejercicio.h"

/**
 * c_function do the same of ASM function for compare purpose
 * 
 * @param * vectorIn cada elemto se multiplica x escalar
 * @param * vectorOut vector de salida
 * @param longitud
 * @param escalar factor de multiplicacion
 */
void c_productoEscalar16 (uint16_t * vectorIn, uint16_t * vectorOut, uint32_t longitud, uint32_t escalar);

#endif /* __EJERCICIO_C_FUNCTION_H__ */
